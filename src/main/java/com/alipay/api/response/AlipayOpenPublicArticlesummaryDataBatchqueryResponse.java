package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.ArticleSummaryAnalysisData;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.open.public.articlesummary.data.batchquery response.
 *
 * @author auto create
 * @since 1.0, 2017-12-06 11:28:42
 */
public class AlipayOpenPublicArticlesummaryDataBatchqueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 7625753421465271546L;

	/**
	 * 文章分析数据列表
	 */
	@ApiListField("data_list")
	@ApiField("article_summary_analysis_data")
	private List<ArticleSummaryAnalysisData> dataList;

	public List<ArticleSummaryAnalysisData> getDataList() {
		return this.dataList;
	}

	public void setDataList(List<ArticleSummaryAnalysisData> dataList) {
		this.dataList = dataList;
	}

}
