package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.menu.get response.
 *
 * @author auto create
 * @since 1.0, 2016-01-05 22:34:31
 */
public class AlipayMobilePublicMenuGetResponse extends AlipayResponse {

	private static final long serialVersionUID = 3281792854351936135L;

	/**
	 * success
	 */
	@ApiField("code")
	private String code;

	/**
	 * 菜单内容
	 */
	@ApiField("menu_content")
	private String menuContent;

	/**
	 * 成功
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	public String getMenuContent() {
		return this.menuContent;
	}

	public void setMenuContent(String menuContent) {
		this.menuContent = menuContent;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
