package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.CraftsmanWorkOpenModel;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: koubei.craftsman.data.work.batchquery response.
 *
 * @author auto create
 * @since 1.0, 2017-10-11 20:35:17
 */
public class KoubeiCraftsmanDataWorkBatchqueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 3494817734166518571L;

	/**
	 * 当前页码
	 */
	@ApiField("current_page_no")
	private Long currentPageNo;

	/**
	 * 每页记录数
	 */
	@ApiField("page_size")
	private Long pageSize;

	/**
	 * 总页码数目
	 */
	@ApiField("total_page_no")
	private Long totalPageNo;

	/**
	 * 总共手艺人作品数目
	 */
	@ApiField("total_works")
	private Long totalWorks;

	/**
	 * 作品信息列表
	 */
	@ApiListField("works")
	@ApiField("craftsman_work_open_model")
	private List<CraftsmanWorkOpenModel> works;

	public Long getCurrentPageNo() {
		return this.currentPageNo;
	}

	public void setCurrentPageNo(Long currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public Long getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(Long pageSize) {
		this.pageSize = pageSize;
	}

	public Long getTotalPageNo() {
		return this.totalPageNo;
	}

	public void setTotalPageNo(Long totalPageNo) {
		this.totalPageNo = totalPageNo;
	}

	public Long getTotalWorks() {
		return this.totalWorks;
	}

	public void setTotalWorks(Long totalWorks) {
		this.totalWorks = totalWorks;
	}

	public List<CraftsmanWorkOpenModel> getWorks() {
		return this.works;
	}

	public void setWorks(List<CraftsmanWorkOpenModel> works) {
		this.works = works;
	}

}
