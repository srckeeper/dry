package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.FengdieSpaceDetailModel;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.marketing.tool.fengdie.space.create response.
 *
 * @author auto create
 * @since 1.0, 2017-10-16 20:18:42
 */
public class AlipayMarketingToolFengdieSpaceCreateResponse extends AlipayResponse {

	private static final long serialVersionUID = 6138433152294911661L;

	/**
	 * 创建成功后返回空间的基本信息，包含空间 ID、标题、创建时间与可用域名列表
	 */
	@ApiField("data")
	private FengdieSpaceDetailModel data;

	public FengdieSpaceDetailModel getData() {
		return this.data;
	}

	public void setData(FengdieSpaceDetailModel data) {
		this.data = data;
	}

}
