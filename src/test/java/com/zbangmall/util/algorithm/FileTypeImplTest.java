package com.zbangmall.util.algorithm;

import com.zbangmall.util.common.TestUtil;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.File;

public class FileTypeImplTest extends TestCase {

	@Test
	public void testFileType() {
		assertEquals("gif", FileTypeImpl.getFileType(new File(TestUtil.path + "ali.gif")));
		assertEquals("png", FileTypeImpl.getFileType(new File(TestUtil.path + "tgepng")));
	}

}