package com.zbangmall.util.crypt.digest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class DigestUtil {


	/**
	 * 对文件全文生成MD5摘要
	 *
	 * @param file 要加密的文件
	 * @return MD5摘要码
	 */
	public static String getMD5(File file) {
		FileInputStream fis = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			fis = new FileInputStream(file);
			byte[] buffer = new byte[2048];
			int    length = -1;
			while ((length = fis.read(buffer)) != -1) {
				md.update(buffer, 0, length);
			}
			byte[] b = md.digest();
			return byteToHexString(b);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return null;
	}

	/**
	 * 对一段String生成MD5加密信息
	 * 摘要的结果是16个字节，转成十六进制是32个字节
	 *
	 * @param message 要加密的String
	 * @return 生成的MD5信息
	 */
	public static String getMD5(String message) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[]        b  = md.digest(message.getBytes());
			return byteToHexString(b);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 对一段String生成SHA1加密信息
	 * 摘要的结果是20个字节，转成十六进制是40个字节
	 *
	 * @param message 要加密的String
	 * @return 生成的SHA1信息
	 */
	public static String getSHA1(String message) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
			byte[]        b  = md.digest(message.getBytes());
			return byteToHexString(b);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 对一段String生成SHA256加密信息
	 * 摘要的结果是32个字节，转成十六进制是64个字节
	 *
	 * @param message 要加密的String
	 * @return 生成的SHA1信息
	 */
	public static String getSHA256(String message) {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			byte[]        b  = md.digest(message.getBytes());
			return byteToHexString(b);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * 把byte[]数组转换成十六进制字符串表示形式
	 */
	public static String byteToHexString(byte[] bytes) {
		StringBuffer sb = new StringBuffer();
		for (byte b : bytes) {
			int    i   = b & 0xff;
			String hex = Integer.toHexString(i);
			if (hex.length() == 1) {
				sb.append("0");
			}
			sb.append(hex);
		}
		return sb.toString();
	}

}
