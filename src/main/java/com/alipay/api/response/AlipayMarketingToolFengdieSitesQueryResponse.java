package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.FengdieSitesQueryRespModel;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.marketing.tool.fengdie.sites.query response.
 *
 * @author auto create
 * @since 1.0, 2017-10-16 20:19:23
 */
public class AlipayMarketingToolFengdieSitesQueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 5295247798122458819L;

	/**
	 * 站点查询返回值
	 */
	@ApiField("data")
	private FengdieSitesQueryRespModel data;

	public FengdieSitesQueryRespModel getData() {
		return this.data;
	}

	public void setData(FengdieSitesQueryRespModel data) {
		this.data = data;
	}

}
