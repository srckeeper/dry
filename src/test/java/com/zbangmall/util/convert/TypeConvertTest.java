package com.zbangmall.util.convert;

import org.junit.Test;

import java.util.List;
import java.util.Map;

public class TypeConvertTest {
	String jsonStr = "{\"id\":\"0375\",\"city\":\"平顶山\"}";
	String xmlStr  = "<xml>\n" +
			" <ToUserName><![CDATA[toUser]]></ToUserName>\n" +
			" <FromUserName><![CDATA[fromUser]]></FromUserName>\n" +
			" <CreateTime>1348831860</CreateTime>\n" +
			" <MsgType><![CDATA[text]]></MsgType>\n" +
			" <Content><![CDATA[你好]]></Content>\n" +
			" <MsgId>1234567890123456</MsgId>\n" +
			" </xml>";

	@Test
	public void toMap() throws Exception {
//        Map<String, Object> mapJson = TypeConvert.toMap(jsonStr, TypeConvert.TypeEnum.JSON);
//        for (Map.Entry<String, Object> stringStringEntry : mapJson.entrySet()) {
//            System.out.println(stringStringEntry.getKey() + "=" + stringStringEntry.getValue());
//        }
//        System.out.println("--------------------------------------------------");
//        Map<String, Object> mapXml = TypeConvert.toMap(xmlStr, TypeConvert.TypeEnum.XML);
//        for (Map.Entry<String, Object> stringStringEntry : mapXml.entrySet()) {
//            System.out.println(stringStringEntry.getKey() + "=" + stringStringEntry.getValue());
//        }
		String              json    = "{\"key\":[\"187473178f364f06ae4688eb5f82b04b\",\"2e972a86bf6b4cc2a01a59a81aabf501\",\"4ef6f24af9074419b87e3982f0702205\",\"f95fe7e0769f4a03a17b7cecd5a1ab47\",\"5caa82d318cf4b7e97738b13b16af753\",\"1d2440cf154b460b9e2970da9ba89f67\",\"4a8a2963b84142278e92108af4311275\",\"4fd18e63914f422992ee88ed39240b2c\",\"615aa1f8d8b440ddbb89b79e3fba7acd\",\"7d2d69fd8e6b405f9f31ff1298848690\",\"89e8874fa39f4d37a904543c4577624d\",\"c11076d8e6f04e918e5370a185f9df0d\",\"e97bbc68d6174749be78aa3de5ea641f\",\"6582845048334471ac84908504fc342d\",\"367ffffb32cd40c48043e432ee19bac7\",\"437e050b3e8f4ee18819f72e4cb0eca7\",\"4e0cae79fb6f48d4b12927f1e7d5be2d\",\"5ee7e71ab2a6431396fa9afa074f1c49\",\"6357179226d340598710dbede1939eef\",\"bda426611ed44f669a895bbd5be17dea\",\"ea0a1b0b256442fa95d2079d84a237d3\",\"74f140bb07ef441d9f18d20d556bd482\",\"80b213426bbe4e48b14c7a7cbf7f8c69\",\"afbcf9f5e5ad4a3cb1d253bf17e0565c\",\"c95ef3aba8724e29b736e00dfe5c4607\",\"f0fee2ae3e854e41a08e645d5c4e32c1\"]}";
		Map<String, Object> mapJson = TypeConvert.toMap(json, TypeConvert.TypeEnum.JSON);
		for (Map.Entry<String, Object> entry : mapJson.entrySet()) {
			List<String> list = (List<String>) entry.getValue();
			System.out.println(list.size());
		}
	}

	@Test
	public void toStr() throws Exception {
//        Map<String, String> map = TypeConvert.toObj(Map.class, jsonStr, TypeConvert.TypeEnum.JSON);
//
//        String json = TypeConvert.toStr(map, TypeConvert.TypeEnum.JSON);
//        System.out.println(json);
//        System.out.println("--------------------------------------------------");
//
//        String xml = TypeConvert.toStr(map, TypeConvert.TypeEnum.XML);
//        System.out.println(xml);
//        System.out.println("--------------------------------------------------");
//
//        String                 json2xml = TypeConvert.toStr(jsonStr, TypeConvert.TypeEnum.XML);
//        System.out.println(json2xml);
//        System.out.println("--------------------------------------------------");
//
//        String xml2json = TypeConvert.toStr(xmlStr, TypeConvert.TypeEnum.JSON);
//        System.out.println(xml2json);

	}


	@Test
	public void toObj() throws Exception {
		Map<String, String> mapJson = TypeConvert.toObj(Map.class, jsonStr, TypeConvert.TypeEnum.JSON);
		for (Map.Entry<String, String> stringStringEntry : mapJson.entrySet()) {
			System.out.println(stringStringEntry.getKey() + "=" + stringStringEntry.getValue());
		}

		System.out.println("--------------------------------------------------");

		Map<String, String> mapXml = TypeConvert.toObj(Map.class, xmlStr, TypeConvert.TypeEnum.XML);
		for (Map.Entry<String, String> stringStringEntry : mapXml.entrySet()) {
			System.out.println(stringStringEntry.getKey() + "=" + stringStringEntry.getValue());
		}
	}

	@Test
	public void toDoc() throws Exception {

	}

}