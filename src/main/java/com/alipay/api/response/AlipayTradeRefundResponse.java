package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.TradeFundBill;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.Date;
import java.util.List;

/**
 * ALIPAY API: alipay.trade.refund response.
 *
 * @author auto create
 * @since 1.0, 2017-12-12 18:09:26
 */
public class AlipayTradeRefundResponse extends AlipayResponse {

	private static final long serialVersionUID = 6613973425862654212L;

	/**
	 * 用户的登录id
	 */
	@ApiField("buyer_logon_id")
	private String buyerLogonId;

	/**
	 * 买家在支付宝的用户id
	 */
	@ApiField("buyer_user_id")
	private String buyerUserId;

	/**
	 * 本次退款是否发生了资金变化
	 */
	@ApiField("fund_change")
	private String fundChange;

	/**
	 * 退款支付时间
	 */
	@ApiField("gmt_refund_pay")
	private Date gmtRefundPay;

	/**
	 * 买家支付宝用户号，该参数已废弃，请不要使用
	 */
	@ApiField("open_id")
	private String openId;

	/**
	 * 商户订单号
	 */
	@ApiField("out_trade_no")
	private String outTradeNo;

	/**
	 * 本次退款金额中买家退款金额
	 */
	@ApiField("present_refund_buyer_amount")
	private String presentRefundBuyerAmount;

	/**
	 * 本次退款金额中平台优惠退款金额
	 */
	@ApiField("present_refund_discount_amount")
	private String presentRefundDiscountAmount;

	/**
	 * 本次退款金额中商家优惠退款金额
	 */
	@ApiField("present_refund_mdiscount_amount")
	private String presentRefundMdiscountAmount;

	/**
	 * 退款使用的资金渠道
	 */
	@ApiListField("refund_detail_item_list")
	@ApiField("trade_fund_bill")
	private List<TradeFundBill> refundDetailItemList;

	/**
	 * 退款总金额
	 */
	@ApiField("refund_fee")
	private String refundFee;

	/**
	 * 本次商户实际退回金额
	 * 注：在签约收单产品时需勾选“返回资金明细”才会返回
	 */
	@ApiField("send_back_fee")
	private String sendBackFee;

	/**
	 * 交易在支付时候的门店名称
	 */
	@ApiField("store_name")
	private String storeName;

	/**
	 * 2013112011001004330000121536
	 */
	@ApiField("trade_no")
	private String tradeNo;

	public String getBuyerLogonId() {
		return this.buyerLogonId;
	}

	public void setBuyerLogonId(String buyerLogonId) {
		this.buyerLogonId = buyerLogonId;
	}

	public String getBuyerUserId() {
		return this.buyerUserId;
	}

	public void setBuyerUserId(String buyerUserId) {
		this.buyerUserId = buyerUserId;
	}

	public String getFundChange() {
		return this.fundChange;
	}

	public void setFundChange(String fundChange) {
		this.fundChange = fundChange;
	}

	public Date getGmtRefundPay() {
		return this.gmtRefundPay;
	}

	public void setGmtRefundPay(Date gmtRefundPay) {
		this.gmtRefundPay = gmtRefundPay;
	}

	public String getOpenId() {
		return this.openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getOutTradeNo() {
		return this.outTradeNo;
	}

	public void setOutTradeNo(String outTradeNo) {
		this.outTradeNo = outTradeNo;
	}

	public String getPresentRefundBuyerAmount() {
		return this.presentRefundBuyerAmount;
	}

	public void setPresentRefundBuyerAmount(String presentRefundBuyerAmount) {
		this.presentRefundBuyerAmount = presentRefundBuyerAmount;
	}

	public String getPresentRefundDiscountAmount() {
		return this.presentRefundDiscountAmount;
	}

	public void setPresentRefundDiscountAmount(String presentRefundDiscountAmount) {
		this.presentRefundDiscountAmount = presentRefundDiscountAmount;
	}

	public String getPresentRefundMdiscountAmount() {
		return this.presentRefundMdiscountAmount;
	}

	public void setPresentRefundMdiscountAmount(String presentRefundMdiscountAmount) {
		this.presentRefundMdiscountAmount = presentRefundMdiscountAmount;
	}

	public List<TradeFundBill> getRefundDetailItemList() {
		return this.refundDetailItemList;
	}

	public void setRefundDetailItemList(List<TradeFundBill> refundDetailItemList) {
		this.refundDetailItemList = refundDetailItemList;
	}

	public String getRefundFee() {
		return this.refundFee;
	}

	public void setRefundFee(String refundFee) {
		this.refundFee = refundFee;
	}

	public String getSendBackFee() {
		return this.sendBackFee;
	}

	public void setSendBackFee(String sendBackFee) {
		this.sendBackFee = sendBackFee;
	}

	public String getStoreName() {
		return this.storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getTradeNo() {
		return this.tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

}
