package com.zbangmall.wechat;

import com.zbangmall.wechat.domain.Code4AccessToken;
import com.zbangmall.wechat.domain.WechatUserInfo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class WechatLoginClientDemo {


	//region 微信公众号授权，获取用户信息 *********************************************
	@RequestMapping(value = "wechat/oauth_official_acc", method = RequestMethod.GET)
	public void wxAuthOfficialAccWithState(HttpServletResponse response) throws Exception {
		String code = "我是A会员推荐过来的!";
		String url  = WechatLoginClient.getOfficialAccAuthUrl(code);
		response.sendRedirect(url);
	}

	@RequestMapping(value = "wechat/oauth_callback_official_acc", method = RequestMethod.GET)
	public void wxAuthOfficialAccWithState(HttpServletRequest request) throws Exception {
		Code4AccessToken code4AccessToken = WechatLoginClient.getCode4AccessToken(request);
		WechatUserInfo   user             = WechatLoginClient.getUserInfoByCode4OffcialAcc(code4AccessToken);
		//我是A会员推荐过来的!
		System.out.println(code4AccessToken.getState());
		System.out.println(user);
	}
	//endregion 微信公众号授权，获取用户信息 *********************************************


	//region pc端授权，获取用户信息 *********************************************
	@RequestMapping(value = "wechat/oauth_pc", method = RequestMethod.GET)
	public void wxAuthPcWithState(HttpServletResponse response) throws Exception {
		String code = "我是A会员推荐过来的!";
		String url  = WechatLoginClient.getPcAuthUrl(code);
		response.sendRedirect(url);
	}

	@RequestMapping(value = "wechat/oauth_callback_pc", method = RequestMethod.GET)
	public void wxAuthPcWithState(HttpServletRequest request) throws Exception {
		Code4AccessToken code4AccessToken = WechatLoginClient.getCode4AccessToken(request);
		WechatUserInfo   user             = WechatLoginClient.getUserInfoByCode4PC(code4AccessToken);
		//我是A会员推荐过来的!
		System.out.println(code4AccessToken.getState());
		System.out.println(user);
	}
	//endregion pc端授权，获取用户信息 *********************************************


	//region app端授权，获取用户信息 *********************************************


	//endregion app端授权，获取用户信息 *********************************************

}
