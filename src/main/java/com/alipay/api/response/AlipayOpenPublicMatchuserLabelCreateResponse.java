package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.ErrorMatcher;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.open.public.matchuser.label.create response.
 *
 * @author auto create
 * @since 1.0, 2017-07-04 11:09:47
 */
public class AlipayOpenPublicMatchuserLabelCreateResponse extends AlipayResponse {

	private static final long serialVersionUID = 1425144283731153535L;

	/**
	 * 用户打标失败数量
	 */
	@ApiField("error_count")
	private Long errorCount;

	/**
	 * 出错的匹配器列表
	 */
	@ApiListField("error_matchers")
	@ApiField("error_matcher")
	private List<ErrorMatcher> errorMatchers;

	public Long getErrorCount() {
		return this.errorCount;
	}

	public void setErrorCount(Long errorCount) {
		this.errorCount = errorCount;
	}

	public List<ErrorMatcher> getErrorMatchers() {
		return this.errorMatchers;
	}

	public void setErrorMatchers(List<ErrorMatcher> errorMatchers) {
		this.errorMatchers = errorMatchers;
	}

}
