package com.zbangmall.wechat;

import com.zbangmall.common.WechatUtil;
import org.dom4j.Document;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class WechatReplyClientDemo {
	/**
	 * 微信首次验证开发者的服务器
	 */
	@RequestMapping(value = "weixin/reply", method = RequestMethod.GET)
	public void WxFirstValidateEcs(HttpServletRequest request, HttpServletResponse response) throws IOException {
		//微信服务器发来的
		Map<String, String[]> requestMap = request.getParameterMap();
		WechatUtil.PrintlnWxXml(requestMap);

		//我的服务器发出的
		String echostr = WechatUtil.checkSignature(requestMap);
		WechatUtil.PrintlnWeXml(echostr);
		response.getWriter().println(echostr);
	}

	/**
	 * 开发者服务器接受用户信息的处理分发方法
	 */
	@RequestMapping(value = "weixin/reply", method = RequestMethod.POST)
	public void WxTalk(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//微信服务器发来的
		Document document = WechatUtil.request2Document(request);
		WechatUtil.PrintlnWxXml(document);


		//我的服务器发出的
		String xml_out = WechatReplyClient.documentHandler(document);
		WechatUtil.PrintlnWeXml(xml_out);

		response.getWriter().println(xml_out);
	}
}
