package com.zbangmall.util.crypt.asymmetric;

import org.junit.Test;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;


public class RsaCryptTest {

	@Test
	public void publicDecrypt() {
		//==>>>准备测试数据
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 10; i++) {
			sb.append("[公钥解密==================分片(加密/解密)]");
		}

		//==>>>测试逻辑
		KeyPair    keyPair       = RsaCrypt.getKeyPair();
		PublicKey  publicKey     = keyPair.getPublic();
		PrivateKey privateKey    = keyPair.getPrivate();
		String     publicKeyStr  = RsaCrypt.getPublicKey(keyPair);
		String     privateKeyStr = RsaCrypt.getPrivateKey(keyPair);

		String str1_0 = RsaCrypt.encryptByPrivateKey(privateKey, sb.toString());
		String str2_0 = RsaCrypt.decryptByPublicKey(publicKey, str1_0);
		String str1_1 = RsaCrypt.encryptByPrivateKey(privateKeyStr, sb.toString());
		String str2_1 = RsaCrypt.decryptByPublicKey(publicKeyStr, str1_1);

		//==>>>测试结果
		System.out.println("解密：" + str2_0);
		System.out.println("解密：" + str2_1);
	}


	@Test
	public void privateDecrypt() {
		//==>>>准备测试数据
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 10; i++) {
			sb.append("[私钥解密==================分片(加密/解密)]");
		}

		//==>>>测试逻辑
		KeyPair    keyPair       = RsaCrypt.getKeyPair();
		PublicKey  publicKey     = keyPair.getPublic();
		PrivateKey privateKey    = keyPair.getPrivate();
		String     publicKeyStr  = RsaCrypt.getPublicKey(keyPair);
		String     privateKeyStr = RsaCrypt.getPrivateKey(keyPair);

		String str1_0 = RsaCrypt.encryptByPublicKey(publicKey, sb.toString());
		String str2_0 = RsaCrypt.decryptByPrivateKey(privateKey, str1_0);
		String str1_1 = RsaCrypt.encryptByPublicKey(publicKeyStr, sb.toString());
		String str2_2 = RsaCrypt.decryptByPrivateKey(privateKeyStr, str1_1);

		//==>>>测试结果
		System.out.println("解密：" + str2_0);
		System.out.println("解密：" + str2_2);
	}


}