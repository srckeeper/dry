package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.ebpp.pdeduct.sign.add response.
 *
 * @author auto create
 * @since 1.0, 2017-10-10 14:30:21
 */
public class AlipayEbppPdeductSignAddResponse extends AlipayResponse {

	private static final long serialVersionUID = 6816877596426165853L;

	/**
	 * 支付宝代扣协议ID
	 */
	@ApiField("agreement_id")
	private String agreementId;

	/**
	 * 支付宝协议状态。签约成功则返回success
	 */
	@ApiField("agreement_status")
	private String agreementStatus;

	/**
	 * 扩展参数，可为空
	 */
	@ApiField("extend_field")
	private String extendField;

	/**
	 * 通知方式设置。
	 */
	@ApiField("notify_config")
	private String notifyConfig;

	/**
	 * 商户生成的代扣协议ID
	 */
	@ApiField("out_agreement_id")
	private String outAgreementId;

	/**
	 * 支付方式设置
	 */
	@ApiListField("pay_config")
	@ApiField("string")
	private List<String> payConfig;

	/**
	 * 签约时间
	 */
	@ApiField("sign_date")
	private String signDate;

	public String getAgreementId() {
		return this.agreementId;
	}

	public void setAgreementId(String agreementId) {
		this.agreementId = agreementId;
	}

	public String getAgreementStatus() {
		return this.agreementStatus;
	}

	public void setAgreementStatus(String agreementStatus) {
		this.agreementStatus = agreementStatus;
	}

	public String getExtendField() {
		return this.extendField;
	}

	public void setExtendField(String extendField) {
		this.extendField = extendField;
	}

	public String getNotifyConfig() {
		return this.notifyConfig;
	}

	public void setNotifyConfig(String notifyConfig) {
		this.notifyConfig = notifyConfig;
	}

	public String getOutAgreementId() {
		return this.outAgreementId;
	}

	public void setOutAgreementId(String outAgreementId) {
		this.outAgreementId = outAgreementId;
	}

	public List<String> getPayConfig() {
		return this.payConfig;
	}

	public void setPayConfig(List<String> payConfig) {
		this.payConfig = payConfig;
	}

	public String getSignDate() {
		return this.signDate;
	}

	public void setSignDate(String signDate) {
		this.signDate = signDate;
	}

}
