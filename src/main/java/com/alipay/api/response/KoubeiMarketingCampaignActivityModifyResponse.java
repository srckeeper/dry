package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: koubei.marketing.campaign.activity.modify response.
 *
 * @author auto create
 * @since 1.0, 2017-12-07 20:43:31
 */
public class KoubeiMarketingCampaignActivityModifyResponse extends AlipayResponse {

	private static final long serialVersionUID = 6891536115521872879L;

	/**
	 * 活动子状态，如审核中
	 */
	@ApiField("audit_status")
	private String auditStatus;

	/**
	 * 活动状态
	 */
	@ApiField("camp_status")
	private String campStatus;

	public String getAuditStatus() {
		return this.auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getCampStatus() {
		return this.campStatus;
	}

	public void setCampStatus(String campStatus) {
		this.campStatus = campStatus;
	}

}
