package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.open.public.group.crowd.query response.
 *
 * @author auto create
 * @since 1.0, 2017-11-10 15:32:41
 */
public class AlipayOpenPublicGroupCrowdQueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 4467717127231229229L;

	/**
	 * 分组圈出的人群数量
	 */
	@ApiField("count")
	private String count;

	public String getCount() {
		return this.count;
	}

	public void setCount(String count) {
		this.count = count;
	}

}
