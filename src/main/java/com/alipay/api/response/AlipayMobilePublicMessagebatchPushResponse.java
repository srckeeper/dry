package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.messagebatch.push response.
 *
 * @author auto create
 * @since 1.0, 2017-04-14 20:29:00
 */
public class AlipayMobilePublicMessagebatchPushResponse extends AlipayResponse {

	private static final long serialVersionUID = 2125873559261365796L;

	/**
	 * 成功
	 */
	@ApiField("code")
	private String code;

	/**
	 * 消息ID
	 */
	@ApiField("data")
	private String data;

	/**
	 * 成功
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
