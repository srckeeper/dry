package com.alipay.api.request;

import com.alipay.api.AlipayObject;
import com.alipay.api.AlipayRequest;
import com.alipay.api.internal.util.AlipayHashMap;
import com.alipay.api.response.AlipayUserAccountSearchResponse;

import java.util.Map;

/**
 * ALIPAY API: alipay.user.account.search request
 *
 * @author auto create
 * @since 1.0, 2016-08-11 18:02:23
 */
public class AlipayUserAccountSearchRequest implements AlipayRequest<AlipayUserAccountSearchResponse> {

	private AlipayHashMap udfParams; // add user-defined text parameters
	private String apiVersion = "1.0";

	/**
	 * 查询的结束时间
	 */
	private String endTime;

	/**
	 * 需要过滤的字符
	 */
	private String fields;

	/**
	 * 查询的页数
	 */
	private String pageNo;

	/**
	 * 每页的条数
	 */
	private String pageSize;

	/**
	 * 查询的开始时间
	 */
	private String startTime;

	/**
	 * 查询账务的类型
	 */
	private String type;
	private String terminalType;
	private String terminalInfo;
	private String prodCode;
	private String notifyUrl;
	private String returnUrl;
	private boolean      needEncrypt = false;
	private AlipayObject bizModel    = null;

	public String getEndTime() {
		return this.endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getFields() {
		return this.fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	public String getPageNo() {
		return this.pageNo;
	}

	public void setPageNo(String pageNo) {
		this.pageNo = pageNo;
	}

	public String getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getStartTime() {
		return this.startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String getNotifyUrl() {
		return this.notifyUrl;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Override
	public String getReturnUrl() {
		return this.returnUrl;
	}

	@Override
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	@Override
	public String getApiVersion() {
		return this.apiVersion;
	}

	@Override
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Override
	public String getTerminalType() {
		return this.terminalType;
	}

	@Override
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@Override
	public String getTerminalInfo() {
		return this.terminalInfo;
	}

	@Override
	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}

	@Override
	public String getProdCode() {
		return this.prodCode;
	}

	@Override
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	@Override
	public String getApiMethodName() {
		return "alipay.user.account.search";
	}

	@Override
	public Map<String, String> getTextParams() {
		AlipayHashMap txtParams = new AlipayHashMap();
		txtParams.put("end_time", this.endTime);
		txtParams.put("fields", this.fields);
		txtParams.put("page_no", this.pageNo);
		txtParams.put("page_size", this.pageSize);
		txtParams.put("start_time", this.startTime);
		txtParams.put("convert", this.type);
		if (udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public void putOtherTextParam(String key, String value) {
		if (this.udfParams == null) {
			this.udfParams = new AlipayHashMap();
		}
		this.udfParams.put(key, value);
	}

	@Override
	public Class<AlipayUserAccountSearchResponse> getResponseClass() {
		return AlipayUserAccountSearchResponse.class;
	}


	@Override
	public boolean isNeedEncrypt() {

		return this.needEncrypt;
	}


	@Override
	public void setNeedEncrypt(boolean needEncrypt) {

		this.needEncrypt = needEncrypt;
	}

	@Override
	public AlipayObject getBizModel() {

		return this.bizModel;
	}


	@Override
	public void setBizModel(AlipayObject bizModel) {

		this.bizModel = bizModel;
	}


}
