package com.zbangmall.wechat.domain;

/**
 * 获取access_token用的code实体类
 */
public class Code4AccessToken extends ErrorDomain {

	private String code;

	private String state;

	public Code4AccessToken(String code, String state) {
		this.code = code;
		this.state = state;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Code4AccessToken{" +
				"code='" + code + '\'' +
				", state='" + state + '\'' +
				'}';
	}
}
