package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: monitor.heartbeat.syn response.
 *
 * @author auto create
 * @since 1.0, 2016-06-06 22:21:41
 */
public class MonitorHeartbeatSynResponse extends AlipayResponse {

	private static final long serialVersionUID = 5461263543498842676L;

	/**
	 * 商户pid
	 */
	@ApiField("pid")
	private String pid;

	public String getPid() {
		return this.pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

}
