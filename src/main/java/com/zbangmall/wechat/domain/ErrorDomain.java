package com.zbangmall.wechat.domain;

/**
 * 错误信息类
 */
public class ErrorDomain {

	/**
	 * 错误码
	 */
	private Integer errcode;
	/**
	 * 错误信息
	 */
	private String  errmsg;

	public Integer getErrcode() {
		return errcode;
	}

	public void setErrcode(Integer errcode) {
		this.errcode = errcode;
	}

	public String getErrmsg() {
		return errmsg;
	}

	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}

	@Override
	public String toString() {
		return "ErrorDomain{" +
				"errcode=" + errcode +
				", errmsg='" + errmsg + '\'' +
				'}';
	}
}
