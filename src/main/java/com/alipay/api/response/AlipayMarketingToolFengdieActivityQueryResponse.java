package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.FengdieActivity;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.marketing.tool.fengdie.activity.query response.
 *
 * @author auto create
 * @since 1.0, 2017-03-20 10:29:11
 */
public class AlipayMarketingToolFengdieActivityQueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 4475354454229277677L;

	/**
	 * H5应用详情
	 */
	@ApiField("activity")
	private FengdieActivity activity;

	public FengdieActivity getActivity() {
		return this.activity;
	}

	public void setActivity(FengdieActivity activity) {
		this.activity = activity;
	}

}
