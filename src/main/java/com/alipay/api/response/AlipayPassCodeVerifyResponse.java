package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.pass.code.verify response.
 *
 * @author auto create
 * @since 1.0, 2014-06-12 17:16:11
 */
public class AlipayPassCodeVerifyResponse extends AlipayResponse {

	private static final long serialVersionUID = 3814169368845618178L;

	/**
	 * 返回核销流水号及操作结果
	 */
	@ApiField("biz_result")
	private String bizResult;

	/**
	 * 返回码.
	 */
	@ApiField("error_code")
	private String errorCode;

	/**
	 * 是否发码成功的标识。
	 */
	@ApiField("success")
	private Boolean success;

	public String getBizResult() {
		return this.bizResult;
	}

	public void setBizResult(String bizResult) {
		this.bizResult = bizResult;
	}

	@Override
	public String getErrorCode() {
		return this.errorCode;
	}

	@Override
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public Boolean getSuccess() {
		return this.success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

}
