package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.MenuAnalysisData;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.open.public.menu.data.batchquery response.
 *
 * @author auto create
 * @since 1.0, 2017-12-19 16:57:33
 */
public class AlipayOpenPublicMenuDataBatchqueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 8724749414885688576L;

	/**
	 * 菜单分析数据列表
	 */
	@ApiListField("data_list")
	@ApiField("menu_analysis_data")
	private List<MenuAnalysisData> dataList;

	public List<MenuAnalysisData> getDataList() {
		return this.dataList;
	}

	public void setDataList(List<MenuAnalysisData> dataList) {
		this.dataList = dataList;
	}

}
