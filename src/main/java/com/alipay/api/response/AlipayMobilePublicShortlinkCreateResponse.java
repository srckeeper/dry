package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.shortlink.create response.
 *
 * @author auto create
 * @since 1.0, 2016-07-29 19:58:48
 */
public class AlipayMobilePublicShortlinkCreateResponse extends AlipayResponse {

	private static final long serialVersionUID = 7182492983389562912L;

	/**
	 * 结果码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 成功
	 */
	@ApiField("msg")
	private String msg;

	/**
	 * 短链接url
	 */
	@ApiField("shortlink")
	private String shortlink;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getShortlink() {
		return this.shortlink;
	}

	public void setShortlink(String shortlink) {
		this.shortlink = shortlink;
	}

}
