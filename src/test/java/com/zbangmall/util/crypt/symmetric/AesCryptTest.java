package com.zbangmall.util.crypt.symmetric;

import org.junit.Test;

public class AesCryptTest {
	@Test
	public void test() throws Exception {
		String str      = "1";
		String password = "1234567812345678";

		String encryptStr = AesCrypt.encrypt(str, password);
		System.out.println("AES加密字符串：" + encryptStr);

		String encryptStrDes = DesCrypt.encrypt(str,

				password);
		System.out.println("DES加密字符串：" + encryptStrDes);

		String decryptStr = AesCrypt.decrypt(encryptStr, password);
		System.out.println("解密字符串：" + decryptStr);
	}

}