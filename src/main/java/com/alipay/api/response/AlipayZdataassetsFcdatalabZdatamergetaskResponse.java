package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.zdataassets.fcdatalab.zdatamergetask response.
 *
 * @author auto create
 * @since 1.0, 2017-04-26 15:14:28
 */
public class AlipayZdataassetsFcdatalabZdatamergetaskResponse extends AlipayResponse {

	private static final long serialVersionUID = 4598827622312628289L;

	/**
	 * 返回结果
	 */
	@ApiField("result")
	private String result;

	public String getResult() {
		return this.result;
	}

	public void setResult(String result) {
		this.result = result;
	}

}
