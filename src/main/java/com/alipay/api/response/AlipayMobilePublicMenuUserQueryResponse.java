package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.menu.user.query response.
 *
 * @author auto create
 * @since 1.0, 2016-01-12 17:25:25
 */
public class AlipayMobilePublicMenuUserQueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 5322984996681727179L;

	/**
	 * 结果码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 菜单唯一标识
	 */
	@ApiField("menu_key")
	private String menuKey;

	/**
	 * 结果码描述
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	public String getMenuKey() {
		return this.menuKey;
	}

	public void setMenuKey(String menuKey) {
		this.menuKey = menuKey;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
