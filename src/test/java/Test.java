import java.awt.*;
import java.awt.event.KeyEvent;

public class Test {


	public static void main(String[] args) throws Exception {
		Robot robot = new Robot();

		//按键
		robot.keyPress(KeyEvent.VK_0);
		robot.keyRelease(KeyEvent.VK_0);

		//鼠标
		robot.mouseMove(200, 700);
	}


	@org.junit.Test
	public void methodName() {
		String clazz  = Thread.currentThread().getStackTrace()[1].getClassName();
		String method = Thread.currentThread().getStackTrace()[1].getMethodName();
		int    line   = Thread.currentThread().getStackTrace()[1].getLineNumber();
		System.out.println("全类名: " + clazz + " 方法名: " + method + " 行号: " + line);
	}
}
