package com.zbangmall.pay;

import com.zbangmall.common.SnGenerator;
import com.zbangmall.pay.wechat.WechatPayAssistant;
import com.zbangmall.pay.wechat.domain.WechatPayInfo;
import com.zbangmall.pay.wechat.domain.WechatPreOrderInfo;
import com.zbangmall.pay.wechat.utils.WechatPayConfigurations;
import com.zbangmall.pay.wechat.utils.WechatPayConst;
import com.zbangmall.util.convert.TypeConvert;
import org.junit.Test;

import java.util.Map;


public class PayAssistantTest {
	@Test
	public void preOrder() throws Exception {
		WechatPayInfo payInfo = new WechatPayInfo("BODY", SnGenerator.getSn(), 1,
				WechatPayConfigurations.getNotifyUrl("video"), WechatPayConst.TRADE_TYPE_OFFICIAL_ACCOUNT, "127.0.0.1");
		payInfo.setOpenid("oPFbNv9zgaieN3ajhneTvMqaZ5cI");

		WechatPreOrderInfo  wechatPreOrderInfo = WechatPayAssistant.preOrder(payInfo);
		Map<String, Object> map                = WechatPayAssistant.sign4WechatOfficialAcc(wechatPreOrderInfo);
		String              json               = TypeConvert.toStr(map, TypeConvert.TypeEnum.JSON);
		System.out.println(json);
	}


}

