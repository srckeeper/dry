package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.marketing.cashvoucher.template.create response.
 *
 * @author auto create
 * @since 1.0, 2017-09-21 17:04:08
 */
public class AlipayMarketingCashvoucherTemplateCreateResponse extends AlipayResponse {

	private static final long serialVersionUID = 4227438797133212431L;

	/**
	 * 模板支付确认链接
	 */
	@ApiField("confirm_uri")
	private String confirmUri;

	/**
	 * 资金订单号，模板支付时需要
	 */
	@ApiField("fund_order_no")
	private String fundOrderNo;

	/**
	 * 券模板ID
	 */
	@ApiField("template_id")
	private String templateId;

	public String getConfirmUri() {
		return this.confirmUri;
	}

	public void setConfirmUri(String confirmUri) {
		this.confirmUri = confirmUri;
	}

	public String getFundOrderNo() {
		return this.fundOrderNo;
	}

	public void setFundOrderNo(String fundOrderNo) {
		this.fundOrderNo = fundOrderNo;
	}

	public String getTemplateId() {
		return this.templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

}
