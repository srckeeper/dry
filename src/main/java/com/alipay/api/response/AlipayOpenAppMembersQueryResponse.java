package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.AppMemberInfo;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.open.app.members.query response.
 *
 * @author auto create
 * @since 1.0, 2017-12-20 17:08:51
 */
public class AlipayOpenAppMembersQueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 3855889243543312136L;

	/**
	 * 小程序成员模型
	 */
	@ApiListField("app_member_info_list")
	@ApiField("app_member_info")
	private List<AppMemberInfo> appMemberInfoList;

	public List<AppMemberInfo> getAppMemberInfoList() {
		return this.appMemberInfoList;
	}

	public void setAppMemberInfoList(List<AppMemberInfo> appMemberInfoList) {
		this.appMemberInfoList = appMemberInfoList;
	}

}
