package com.zbangmall.util.common;


import com.github.binarywang.java.emoji.EmojiConverter;

public class EmojiUtil {
	private static EmojiConverter emojiConverter = EmojiConverter.getInstance();

	/**
	 * 将emojiStr转为 带有表情的字符
	 *
	 * @param emojiStr
	 * @return
	 */
	public static String emojiConverterUnicodeStr(String emojiStr) {
		String result = emojiConverter.toUnicode(emojiStr);
		return result;
	}

	/**
	 * 带有表情的字符串转换为编码
	 *
	 * @param str
	 * @return
	 */
	public static String emojiConverterToAlias(String str) {
		String result = emojiConverter.toAlias(str);
		return result;
	}


	//
//        //要用别名替换在字符串中找到的所有表情符号的unicode
//        String str1 = "呵呵An！！An 😀awesome 😃string with a few 😉哈哈!";
//        String result1 = EmojiParser.parseToAliases(str1);
//        System.out.println(result1);
//
//        //用HTML表示 替换字符串中找到的所有表情符号
//        String str2 = "呵呵An ！😀awesome 😃string with a few 😉哈哈!";
//        String resultHexadecimal = EmojiParser.parseToHtmlHexadecimal(str2);
//        System.out.println(resultHexadecimal);
//        System.out.println(PropertiesUtil.propsWechat.getProperty("wechat.official_acc.redirect_url"));
	public static String emojiConverterToHtml(String str) {
		String result = emojiConverter.toHtml(str);
		return result;
	}


}
