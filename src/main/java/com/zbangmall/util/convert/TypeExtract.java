package com.zbangmall.util.convert;


import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * 类型提取处理类
 */
public class TypeExtract {
	/**
	 * HttpServletRequest对象 提取出 xml字符串
	 */
	public static String extXml(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();
		try (
				ServletInputStream in = request.getInputStream();
				InputStreamReader inputStream = new InputStreamReader(in);
				BufferedReader buffer = new BufferedReader(inputStream)
		) {
			String line = null;
			while ((line = buffer.readLine()) != null) {
				sb.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}
