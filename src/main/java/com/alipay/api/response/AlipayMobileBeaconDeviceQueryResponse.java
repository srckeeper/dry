package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.BeaconDeviceInfo;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.beacon.device.query response.
 *
 * @author auto create
 * @since 1.0, 2017-02-28 11:12:47
 */
public class AlipayMobileBeaconDeviceQueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 6466472288723888131L;

	/**
	 * 蓝牙设备信息
	 */
	@ApiField("beacon_device_info")
	private BeaconDeviceInfo beaconDeviceInfo;

	/**
	 * 操作返回码，200为成功
	 */
	@ApiField("code")
	private String code;

	/**
	 * 请求处理结果
	 */
	@ApiField("msg")
	private String msg;

	public BeaconDeviceInfo getBeaconDeviceInfo() {
		return this.beaconDeviceInfo;
	}

	public void setBeaconDeviceInfo(BeaconDeviceInfo beaconDeviceInfo) {
		this.beaconDeviceInfo = beaconDeviceInfo;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
