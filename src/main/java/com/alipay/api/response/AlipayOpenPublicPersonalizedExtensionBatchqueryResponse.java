package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.QueryExtension;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.open.public.personalized.extension.batchquery response.
 *
 * @author auto create
 * @since 1.0, 2017-08-07 18:03:59
 */
public class AlipayOpenPublicPersonalizedExtensionBatchqueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 8669932464348674888L;

	/**
	 * 扩展区套数
	 */
	@ApiField("count")
	private Long count;

	/**
	 * 扩展区信息
	 */
	@ApiListField("extensions")
	@ApiField("query_extension")
	private List<QueryExtension> extensions;

	public Long getCount() {
		return this.count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public List<QueryExtension> getExtensions() {
		return this.extensions;
	}

	public void setExtensions(List<QueryExtension> extensions) {
		this.extensions = extensions;
	}

}
