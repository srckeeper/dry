package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.BenefitInfo;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.mpointprod.benefit.detail.get response.
 *
 * @author auto create
 * @since 1.0, 2015-01-29 15:46:36
 */
public class AlipayMpointprodBenefitDetailGetResponse extends AlipayResponse {

	private static final long serialVersionUID = 1117949593356198587L;

	/**
	 * 权益详情列表
	 */
	@ApiListField("benefit_infos")
	@ApiField("benefit_info")
	private List<BenefitInfo> benefitInfos;

	/**
	 * 响应码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 响应描述
	 */
	@ApiField("msg")
	private String msg;

	public List<BenefitInfo> getBenefitInfos() {
		return this.benefitInfos;
	}

	public void setBenefitInfos(List<BenefitInfo> benefitInfos) {
		this.benefitInfos = benefitInfos;
	}

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
