package com.alipay.api.request;

import com.alipay.api.AlipayObject;
import com.alipay.api.AlipayRequest;
import com.alipay.api.internal.util.AlipayHashMap;
import com.alipay.api.response.AlipayCommerceCityfacilitatorDepositCancelResponse;

import java.util.Map;

/**
 * ALIPAY API: alipay.commerce.cityfacilitator.deposit.cancel request
 *
 * @author auto create
 * @since 1.0, 2015-12-18 21:35:58
 */
public class AlipayCommerceCityfacilitatorDepositCancelRequest implements AlipayRequest<AlipayCommerceCityfacilitatorDepositCancelResponse> {

	private AlipayHashMap udfParams; // add user-defined text parameters
	private String apiVersion = "1.0";

	/**
	 * 合作渠道可通过该接口补登扣款取消请求，以帮助支付宝将用户的资金退给用户
	 */
	private String bizContent;
	private String terminalType;
	private String terminalInfo;
	private String prodCode;
	private String notifyUrl;
	private String returnUrl;
	private boolean      needEncrypt = false;
	private AlipayObject bizModel    = null;

	public String getBizContent() {
		return this.bizContent;
	}

	public void setBizContent(String bizContent) {
		this.bizContent = bizContent;
	}

	@Override
	public String getNotifyUrl() {
		return this.notifyUrl;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Override
	public String getReturnUrl() {
		return this.returnUrl;
	}

	@Override
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	@Override
	public String getApiVersion() {
		return this.apiVersion;
	}

	@Override
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Override
	public String getTerminalType() {
		return this.terminalType;
	}

	@Override
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@Override
	public String getTerminalInfo() {
		return this.terminalInfo;
	}

	@Override
	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}

	@Override
	public String getProdCode() {
		return this.prodCode;
	}

	@Override
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	@Override
	public String getApiMethodName() {
		return "alipay.commerce.cityfacilitator.deposit.cancel";
	}

	@Override
	public Map<String, String> getTextParams() {
		AlipayHashMap txtParams = new AlipayHashMap();
		txtParams.put("biz_content", this.bizContent);
		if (udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public void putOtherTextParam(String key, String value) {
		if (this.udfParams == null) {
			this.udfParams = new AlipayHashMap();
		}
		this.udfParams.put(key, value);
	}

	@Override
	public Class<AlipayCommerceCityfacilitatorDepositCancelResponse> getResponseClass() {
		return AlipayCommerceCityfacilitatorDepositCancelResponse.class;
	}


	@Override
	public boolean isNeedEncrypt() {

		return this.needEncrypt;
	}


	@Override
	public void setNeedEncrypt(boolean needEncrypt) {

		this.needEncrypt = needEncrypt;
	}

	@Override
	public AlipayObject getBizModel() {

		return this.bizModel;
	}


	@Override
	public void setBizModel(AlipayObject bizModel) {

		this.bizModel = bizModel;
	}


}
