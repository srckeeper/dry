package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.menu.add response.
 *
 * @author auto create
 * @since 1.0, 2016-07-29 20:01:02
 */
public class AlipayMobilePublicMenuAddResponse extends AlipayResponse {

	private static final long serialVersionUID = 4223925846453511464L;

	/**
	 * 结果码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 成功
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
