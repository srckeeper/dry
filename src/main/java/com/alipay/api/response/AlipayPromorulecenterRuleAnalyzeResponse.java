package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.promorulecenter.rule.analyze response.
 *
 * @author auto create
 * @since 1.0, 2017-10-09 17:38:20
 */
public class AlipayPromorulecenterRuleAnalyzeResponse extends AlipayResponse {

	private static final long serialVersionUID = 2817335987118957799L;

	/**
	 * 未通过条件描述信息
	 */
	@ApiField("fail_condition_msg")
	private String failConditionMsg;

	/**
	 * 未通过的条件
	 */
	@ApiField("fail_condition_name")
	private String failConditionName;

	/**
	 * 结果码
	 */
	@ApiField("result_code")
	private String resultCode;

	/**
	 * 服务调用是否成功
	 */
	@ApiField("success")
	private String success;

	/**
	 * 规则是否通过
	 */
	@ApiField("triggered")
	private String triggered;

	public String getFailConditionMsg() {
		return this.failConditionMsg;
	}

	public void setFailConditionMsg(String failConditionMsg) {
		this.failConditionMsg = failConditionMsg;
	}

	public String getFailConditionName() {
		return this.failConditionName;
	}

	public void setFailConditionName(String failConditionName) {
		this.failConditionName = failConditionName;
	}

	public String getResultCode() {
		return this.resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getSuccess() {
		return this.success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getTriggered() {
		return this.triggered;
	}

	public void setTriggered(String triggered) {
		this.triggered = triggered;
	}

}
