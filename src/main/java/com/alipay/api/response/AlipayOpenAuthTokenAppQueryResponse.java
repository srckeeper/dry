package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.Date;
import java.util.List;

/**
 * ALIPAY API: alipay.open.auth.token.app.query response.
 *
 * @author auto create
 * @since 1.0, 2016-07-18 13:35:47
 */
public class AlipayOpenAuthTokenAppQueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 3224985156652556431L;

	/**
	 * 授权商户的appid
	 */
	@ApiField("auth_app_id")
	private String authAppId;

	/**
	 * 授权失效时间
	 */
	@ApiField("auth_end")
	private Date authEnd;

	/**
	 * 当前app_auth_token的授权接口列表
	 */
	@ApiListField("auth_methods")
	@ApiField("string")
	private List<String> authMethods;

	/**
	 * 授权生效时间
	 */
	@ApiField("auth_start")
	private Date authStart;

	/**
	 * 应用授权令牌失效时间，单位到秒
	 */
	@ApiField("expires_in")
	private Long expiresIn;

	/**
	 * valid：有效状态；invalid：无效状态
	 */
	@ApiField("status")
	private String status;

	/**
	 * 授权商户的user_id
	 */
	@ApiField("user_id")
	private String userId;

	public String getAuthAppId() {
		return this.authAppId;
	}

	public void setAuthAppId(String authAppId) {
		this.authAppId = authAppId;
	}

	public Date getAuthEnd() {
		return this.authEnd;
	}

	public void setAuthEnd(Date authEnd) {
		this.authEnd = authEnd;
	}

	public List<String> getAuthMethods() {
		return this.authMethods;
	}

	public void setAuthMethods(List<String> authMethods) {
		this.authMethods = authMethods;
	}

	public Date getAuthStart() {
		return this.authStart;
	}

	public void setAuthStart(Date authStart) {
		this.authStart = authStart;
	}

	public Long getExpiresIn() {
		return this.expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
