package com.zbangmall.util.crypt.symmetric;

import org.junit.Test;

public class DesCryptTest {
	@Test
	public void test() throws Exception {
		String str      = "我";
		String password = "12345678";

		String encryptStr = DesCrypt.encrypt(str, password);
		System.out.println("加密字符串：" + encryptStr);

		String decryptStr = DesCrypt.decrypt(encryptStr, password);
		System.out.println("解密字符串：" + decryptStr);
	}


}