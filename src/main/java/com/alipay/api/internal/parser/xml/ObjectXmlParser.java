package com.alipay.api.internal.parser.xml;

import com.alipay.api.*;
import com.alipay.api.internal.mapping.Converter;

/**
 * 单个JSON对象解释器。
 *
 * @author carver.gu
 * @since 1.0, Apr 11, 2010
 */
public class ObjectXmlParser<T extends AlipayResponse> implements AlipayParser<T> {

	private Class<T> clazz;

	public ObjectXmlParser(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Override
	public T parse(String rsp) throws AlipayApiException {
		Converter converter = new XmlConverter();
		return converter.toResponse(rsp, clazz);
	}

	@Override
	public Class<T> getResponseClass() {
		return clazz;
	}

	/**
	 * @see com.alipay.api.AlipayParser#getSignItem(com.alipay.api.AlipayRequest, String)
	 */
	@Override
	public SignItem getSignItem(AlipayRequest<?> request, String responseBody)
			throws AlipayApiException {

		Converter converter = new XmlConverter();

		return converter.getSignItem(request, responseBody);
	}

	/**
	 * @see com.alipay.api.AlipayParser#encryptSourceData(com.alipay.api.AlipayRequest, String, String, String, String, String)
	 */
	@Override
	public String encryptSourceData(AlipayRequest<?> request, String body, String format,
									String encryptType, String encryptKey, String charset)
			throws AlipayApiException {

		Converter converter = new XmlConverter();

		return converter.encryptSourceData(request, body, format, encryptType, encryptKey,
				charset);
	}

}
