package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.eco.mycar.data.external.send response.
 *
 * @author auto create
 * @since 1.0, 2016-05-12 10:25:11
 */
public class AlipayEcoMycarDataExternalSendResponse extends AlipayResponse {

	private static final long serialVersionUID = 6248962596772532979L;

	/**
	 * 20
	 */
	@ApiField("external_system_name")
	private String externalSystemName;

	/**
	 * outter_response
	 */
	@ApiField("process_result")
	private String processResult;

	public String getExternalSystemName() {
		return this.externalSystemName;
	}

	public void setExternalSystemName(String externalSystemName) {
		this.externalSystemName = externalSystemName;
	}

	public String getProcessResult() {
		return this.processResult;
	}

	public void setProcessResult(String processResult) {
		this.processResult = processResult;
	}

}
