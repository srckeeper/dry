package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.open.auth.industry.platform.create.token response.
 *
 * @author auto create
 * @since 1.0, 2016-05-13 18:02:26
 */
public class AlipayOpenAuthIndustryPlatformCreateTokenResponse extends AlipayResponse {

	private static final long serialVersionUID = 8859688649324549791L;

	/**
	 * 授权码
	 */
	@ApiField("auth_code")
	private String authCode;

	/**
	 * appid
	 */
	@ApiField("requst_app_id")
	private String requstAppId;

	/**
	 * scope
	 */
	@ApiField("scope")
	private String scope;

	public String getAuthCode() {
		return this.authCode;
	}

	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	public String getRequstAppId() {
		return this.requstAppId;
	}

	public void setRequstAppId(String requstAppId) {
		this.requstAppId = requstAppId;
	}

	public String getScope() {
		return this.scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

}
