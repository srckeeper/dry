package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.ItemQueryResponse;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: koubei.item.batchquery response.
 *
 * @author auto create
 * @since 1.0, 2018-01-02 17:35:46
 */
public class KoubeiItemBatchqueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 6435238344479744383L;

	/**
	 * 当前页码
	 */
	@ApiField("current_page_no")
	private String currentPageNo;

	/**
	 * 商品信息
	 */
	@ApiListField("item_infos")
	@ApiField("item_query_response")
	private List<ItemQueryResponse> itemInfos;

	/**
	 * 每页记录数
	 */
	@ApiField("page_size")
	private String pageSize;

	/**
	 * 总共商品数目
	 */
	@ApiField("total_items")
	private String totalItems;

	/**
	 * 总页码数目
	 */
	@ApiField("total_page_no")
	private String totalPageNo;

	public String getCurrentPageNo() {
		return this.currentPageNo;
	}

	public void setCurrentPageNo(String currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public List<ItemQueryResponse> getItemInfos() {
		return this.itemInfos;
	}

	public void setItemInfos(List<ItemQueryResponse> itemInfos) {
		this.itemInfos = itemInfos;
	}

	public String getPageSize() {
		return this.pageSize;
	}

	public void setPageSize(String pageSize) {
		this.pageSize = pageSize;
	}

	public String getTotalItems() {
		return this.totalItems;
	}

	public void setTotalItems(String totalItems) {
		this.totalItems = totalItems;
	}

	public String getTotalPageNo() {
		return this.totalPageNo;
	}

	public void setTotalPageNo(String totalPageNo) {
		this.totalPageNo = totalPageNo;
	}

}
