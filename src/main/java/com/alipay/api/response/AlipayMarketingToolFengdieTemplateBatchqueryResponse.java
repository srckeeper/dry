package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.FengdieTemplateListRespModel;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.marketing.tool.fengdie.template.batchquery response.
 *
 * @author auto create
 * @since 1.0, 2017-10-16 20:22:47
 */
public class AlipayMarketingToolFengdieTemplateBatchqueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 2213688999633969224L;

	/**
	 * 模板详情列表
	 */
	@ApiField("data")
	private FengdieTemplateListRespModel data;

	public FengdieTemplateListRespModel getData() {
		return this.data;
	}

	public void setData(FengdieTemplateListRespModel data) {
		this.data = data;
	}

}
