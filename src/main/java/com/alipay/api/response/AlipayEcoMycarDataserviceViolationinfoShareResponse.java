package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.eco.mycar.dataservice.violationinfo.share response.
 *
 * @author auto create
 * @since 1.0, 2017-09-15 16:30:19
 */
public class AlipayEcoMycarDataserviceViolationinfoShareResponse extends AlipayResponse {

	private static final long serialVersionUID = 2512677756884767423L;

	/**
	 * 车架号
	 */
	@ApiField("body_num")
	private String bodyNum;

	/**
	 * 发动机号
	 */
	@ApiField("engine_num")
	private String engineNum;

	/**
	 * 车辆id
	 */
	@ApiField("vehicle_id")
	private String vehicleId;

	/**
	 * 车牌
	 */
	@ApiField("vi_number")
	private String viNumber;

	public String getBodyNum() {
		return this.bodyNum;
	}

	public void setBodyNum(String bodyNum) {
		this.bodyNum = bodyNum;
	}

	public String getEngineNum() {
		return this.engineNum;
	}

	public void setEngineNum(String engineNum) {
		this.engineNum = engineNum;
	}

	public String getVehicleId() {
		return this.vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getViNumber() {
		return this.viNumber;
	}

	public void setViNumber(String viNumber) {
		this.viNumber = viNumber;
	}

}
