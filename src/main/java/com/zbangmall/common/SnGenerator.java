package com.zbangmall.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 单号生成器
 */
public class SnGenerator {

	private final static char[] NUMS                = "123456789".toCharArray();
	private final static char[] LETTERS             = "QWERTYUIPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm".toCharArray();
	private final static char[] MIX_LETTERS_AND_NUM = "QWERTYUIPASDFGHJKLZXCVBNMqwertyuipasdfghjklzxcvbnm01234567890".toCharArray();

	/**
	 * 生成不带前后缀的字符串，格式为 【日期字符串yyyyMMddHHmmssSSS(17位) + 随机字符串】，长度为count
	 */
	public static String getSn() {
		return getSn(null, null, 18, SnGeneratorEnum.MODE_NUM);
	}

	//region ************************* 生成单号字符串 *************************

	/**
	 * 生成格式化的字符串，格式为 【前缀 + 日期字符串yyyyMMddHHmmssSSS(17位) + 中间随机字符串 + 后缀】。如果前缀+后缀+中间字符串的长度超过count，
	 * 将会压缩中间字符串的长度来满足count
	 */
	public static String getSn(String prefix, String suffix, int count, SnGeneratorEnum mode) {
		if (count <= 17) {
			count = 18;
		}
		int           prefixLen = 0;
		int           suffixLen = 0;
		StringBuilder sb        = new StringBuilder();
		if (prefix != null && (!"".equals(prefix))) {
			prefixLen = prefix.length();
			sb.append(prefix);
		}
		if (suffix != null && (!"".equals(suffix))) {
			suffixLen = suffix.length();
		}
		SimpleDateFormat sdf  = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		String           date = sdf.format(new Date());

		int len = count - prefixLen - suffixLen - date.length();
		if (len > 0) {
			switch (mode) {
				case MODE_NUM:
					date = date + randomNums(len);
					break;
				case MODE_LOWER_STR:
					date = date + randomLowerStr(len);
					break;
				case MODE_UPPER_STR:
					date = date + randomUpperStr(len);
					break;
				case MODE_STR:
					date = date + randomStr(len);
					break;
				case MODE_MIX:
					date = date + randomMix(len);
					break;
				default:
					date = date + randomNums(len);
					break;
			}
		}
		sb.append(date.substring(0, count - prefixLen - suffixLen));
		if (suffixLen > 0) {
			sb.append(suffix);
		}

		return sb.toString();
	}

	/**
	 * 生成写字母和数字随机字符串
	 */
	public static String randomMix(int count) {
		return generator(count, MIX_LETTERS_AND_NUM);
	}

	//endregion ************************* 生成单号字符串 *************************


	//region ************************* 生成各种字符内容的字符串 *************************

	/**
	 * 生成大小写混合字母随机字符串
	 */
	private static String randomStr(int count) {
		return generator(count, LETTERS);
	}

	/**
	 * 生成纯大写字母随机字符串
	 */
	private static String randomUpperStr(int count) {
		return generator(count, LETTERS).toUpperCase();
	}

	/**
	 * 生成纯数字随机字符串
	 */
	private static String randomNums(int count) {
		return generator(count, NUMS);
	}

	/**
	 * 生成纯小写字母随机字符串
	 */
	private static String randomLowerStr(int count) {
		return generator(count, LETTERS).toLowerCase();
	}

	/**
	 * 生成器
	 */
	private static String generator(int count, char[] arr) {
		if (count <= 0) {
			count = 6;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < count; i++) {
			double d     = Math.random();
			int    index = (int) Math.floor(d * arr.length);
			sb.append(arr[index]);
		}

		return sb.toString();
	}

	//endregion ************************* 生成各种字符内容的字符串 *************************

	public enum SnGeneratorEnum {
		MODE_MIX,
		MODE_NUM,
		MODE_STR,
		MODE_LOWER_STR,
		MODE_UPPER_STR;
	}
}
