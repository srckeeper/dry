package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.menu.user.update response.
 *
 * @author auto create
 * @since 1.0, 2016-01-05 22:37:24
 */
public class AlipayMobilePublicMenuUserUpdateResponse extends AlipayResponse {

	private static final long serialVersionUID = 6263772688269646265L;

	/**
	 * 结果码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 结果描述
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
