package com.zbangmall.util.helper;


public interface ObjectProcess<T, E> {
	E process(T t);
}
