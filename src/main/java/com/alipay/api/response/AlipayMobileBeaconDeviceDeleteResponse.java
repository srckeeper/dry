package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.beacon.device.delete response.
 *
 * @author auto create
 * @since 1.0, 2017-02-28 11:14:55
 */
public class AlipayMobileBeaconDeviceDeleteResponse extends AlipayResponse {

	private static final long serialVersionUID = 7276647386354726628L;

	/**
	 * 操作返回码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 请求处理结果
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
