package com.zbangmall.wechat;


import com.zbangmall.common.WechatUtil;
import com.zbangmall.util.convert.TypeConvert;
import com.zbangmall.wechat.domain.WechatMsg;
import org.dom4j.Document;

import java.util.Map;

public class WechatReplyClient {
	public static String documentHandler(Document document) {
		String result = null;
		if (document == null) {
			return result;
		}

		Map<String, String> map_in = WechatUtil.document2Map(document);
		WechatMsg           msg_in = WechatUtil.map2Bean(map_in);

		switch (msg_in.getMsgType()) {
			// 用户发送的消息
			case "text":
				result = "收到消息:" + msg_in.getContent();
				break;
			// 事件
			case "event":
				switch (msg_in.getMsgType()) {
					case "subscribe":
						result = menuText();
						break;
					case "unsubscribe":
						result = "取消关注";
						break;
				}
				break;
		}
		return initText(msg_in.getMsgId(), msg_in.getToUserName(), msg_in.getFromUserName(), result);
	}

	public static String menuText() {
		StringBuilder sb = new StringBuilder();
		sb.append("欢迎您的关注，请按照菜单提示操作：\n\n");
		sb.append("1、公司背景介绍\n");
		sb.append("1、公司业务介绍\n");
		sb.append("回复？调出此菜单。");
		return sb.toString();
	}

	public static String initText(String msgId, String toUserName, String fromUserName, String content) {
		WechatMsg wechatMsg = new WechatMsg();
		wechatMsg.setMsgId(msgId);
		wechatMsg.setContent(content);
		wechatMsg.setToUserName(fromUserName);
		wechatMsg.setFromUserName(toUserName);
		wechatMsg.setMsgType("text");
		wechatMsg.setCreateTime(System.currentTimeMillis());
		return TypeConvert.toStr(wechatMsg, TypeConvert.TypeEnum.XML);
	}
}
