package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.IntelligentPromoShopSummaryInfo;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: koubei.marketing.campaign.intelligent.shop.consult response.
 *
 * @author auto create
 * @since 1.0, 2017-11-17 06:02:44
 */
public class KoubeiMarketingCampaignIntelligentShopConsultResponse extends AlipayResponse {

	private static final long serialVersionUID = 4614814391258632998L;

	/**
	 * 总共项数
	 */
	@ApiField("items")
	private String items;

	/**
	 * 智能营销方案符合标准的门店列表
	 */
	@ApiListField("shops")
	@ApiField("intelligent_promo_shop_summary_info")
	private List<IntelligentPromoShopSummaryInfo> shops;

	public String getItems() {
		return this.items;
	}

	public void setItems(String items) {
		this.items = items;
	}

	public List<IntelligentPromoShopSummaryInfo> getShops() {
		return this.shops;
	}

	public void setShops(List<IntelligentPromoShopSummaryInfo> shops) {
		this.shops = shops;
	}

}
