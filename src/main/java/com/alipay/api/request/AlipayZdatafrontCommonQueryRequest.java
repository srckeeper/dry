package com.alipay.api.request;

import com.alipay.api.AlipayObject;
import com.alipay.api.AlipayRequest;
import com.alipay.api.internal.util.AlipayHashMap;
import com.alipay.api.response.AlipayZdatafrontCommonQueryResponse;

import java.util.Map;

/**
 * ALIPAY API: alipay.zdatafront.common.query request
 *
 * @author auto create
 * @since 1.0, 2017-07-24 16:28:03
 */
public class AlipayZdatafrontCommonQueryRequest implements AlipayRequest<AlipayZdatafrontCommonQueryResponse> {

	private AlipayHashMap udfParams; // add user-defined text parameters
	private String apiVersion = "1.0";

	/**
	 * 如果cacheInterval<=0,就直接从外部获取数据；
	 * 如果cacheInterval>0,就先判断cache中的数据是否过期，如果没有过期就返回cache中的数据，如果过期再从外部获取数据并刷新cache，然后返回数据。
	 * 单位：秒
	 */
	private Long cacheInterval;

	/**
	 * 通用查询的入参
	 */
	private String queryConditions;

	/**
	 * 服务名称请与相关开发负责人联系
	 */
	private String serviceName;

	/**
	 * 访问该服务的业务
	 */
	private String visitBiz;

	/**
	 * 访问该服务的业务线
	 */
	private String visitBizLine;

	/**
	 * 访问该服务的部门名称
	 */
	private String visitDomain;
	private String terminalType;
	private String terminalInfo;
	private String prodCode;
	private String notifyUrl;
	private String returnUrl;
	private boolean      needEncrypt = false;
	private AlipayObject bizModel    = null;

	public Long getCacheInterval() {
		return this.cacheInterval;
	}

	public void setCacheInterval(Long cacheInterval) {
		this.cacheInterval = cacheInterval;
	}

	public String getQueryConditions() {
		return this.queryConditions;
	}

	public void setQueryConditions(String queryConditions) {
		this.queryConditions = queryConditions;
	}

	public String getServiceName() {
		return this.serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getVisitBiz() {
		return this.visitBiz;
	}

	public void setVisitBiz(String visitBiz) {
		this.visitBiz = visitBiz;
	}

	public String getVisitBizLine() {
		return this.visitBizLine;
	}

	public void setVisitBizLine(String visitBizLine) {
		this.visitBizLine = visitBizLine;
	}

	public String getVisitDomain() {
		return this.visitDomain;
	}

	public void setVisitDomain(String visitDomain) {
		this.visitDomain = visitDomain;
	}

	@Override
	public String getNotifyUrl() {
		return this.notifyUrl;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Override
	public String getReturnUrl() {
		return this.returnUrl;
	}

	@Override
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	@Override
	public String getApiVersion() {
		return this.apiVersion;
	}

	@Override
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Override
	public String getTerminalType() {
		return this.terminalType;
	}

	@Override
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@Override
	public String getTerminalInfo() {
		return this.terminalInfo;
	}

	@Override
	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}

	@Override
	public String getProdCode() {
		return this.prodCode;
	}

	@Override
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	@Override
	public String getApiMethodName() {
		return "alipay.zdatafront.common.query";
	}

	@Override
	public Map<String, String> getTextParams() {
		AlipayHashMap txtParams = new AlipayHashMap();
		txtParams.put("cache_interval", this.cacheInterval);
		txtParams.put("query_conditions", this.queryConditions);
		txtParams.put("service_name", this.serviceName);
		txtParams.put("visit_biz", this.visitBiz);
		txtParams.put("visit_biz_line", this.visitBizLine);
		txtParams.put("visit_domain", this.visitDomain);
		if (udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public void putOtherTextParam(String key, String value) {
		if (this.udfParams == null) {
			this.udfParams = new AlipayHashMap();
		}
		this.udfParams.put(key, value);
	}

	@Override
	public Class<AlipayZdatafrontCommonQueryResponse> getResponseClass() {
		return AlipayZdatafrontCommonQueryResponse.class;
	}


	@Override
	public boolean isNeedEncrypt() {

		return this.needEncrypt;
	}


	@Override
	public void setNeedEncrypt(boolean needEncrypt) {

		this.needEncrypt = needEncrypt;
	}

	@Override
	public AlipayObject getBizModel() {

		return this.bizModel;
	}


	@Override
	public void setBizModel(AlipayObject bizModel) {

		this.bizModel = bizModel;
	}


}
