package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.menu.update response.
 *
 * @author auto create
 * @since 1.0, 2016-07-29 19:57:40
 */
public class AlipayMobilePublicMenuUpdateResponse extends AlipayResponse {

	private static final long serialVersionUID = 5265781662592468639L;

	/**
	 * 结果码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 成功
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
