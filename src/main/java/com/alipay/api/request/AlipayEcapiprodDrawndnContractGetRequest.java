package com.alipay.api.request;

import com.alipay.api.AlipayObject;
import com.alipay.api.AlipayRequest;
import com.alipay.api.internal.util.AlipayHashMap;
import com.alipay.api.response.AlipayEcapiprodDrawndnContractGetResponse;

import java.util.Map;

/**
 * ALIPAY API: alipay.ecapiprod.drawndn.contract.get request
 *
 * @author auto create
 * @since 1.0, 2016-03-29 11:34:32
 */
public class AlipayEcapiprodDrawndnContractGetRequest implements AlipayRequest<AlipayEcapiprodDrawndnContractGetResponse> {

	private AlipayHashMap udfParams; // add user-defined text parameters
	private String apiVersion = "1.0";

	/**
	 * 支用编号
	 */
	private String drawndnNo;

	/**
	 * 客户身份证号码，为18位，最后X必须为大写
	 */
	private String entityCode;

	/**
	 * 客户姓名
	 */
	private String entityName;

	/**
	 * 融资平台分配给ISV的编码
	 */
	private String isvCode;

	/**
	 * 融资平台分配给小贷公司的机构编码
	 */
	private String orgCode;
	private String terminalType;
	private String terminalInfo;
	private String prodCode;
	private String notifyUrl;
	private String returnUrl;
	private boolean      needEncrypt = false;
	private AlipayObject bizModel    = null;

	public String getDrawndnNo() {
		return this.drawndnNo;
	}

	public void setDrawndnNo(String drawndnNo) {
		this.drawndnNo = drawndnNo;
	}

	public String getEntityCode() {
		return this.entityCode;
	}

	public void setEntityCode(String entityCode) {
		this.entityCode = entityCode;
	}

	public String getEntityName() {
		return this.entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}

	public String getIsvCode() {
		return this.isvCode;
	}

	public void setIsvCode(String isvCode) {
		this.isvCode = isvCode;
	}

	public String getOrgCode() {
		return this.orgCode;
	}

	public void setOrgCode(String orgCode) {
		this.orgCode = orgCode;
	}

	@Override
	public String getNotifyUrl() {
		return this.notifyUrl;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Override
	public String getReturnUrl() {
		return this.returnUrl;
	}

	@Override
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	@Override
	public String getApiVersion() {
		return this.apiVersion;
	}

	@Override
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Override
	public String getTerminalType() {
		return this.terminalType;
	}

	@Override
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@Override
	public String getTerminalInfo() {
		return this.terminalInfo;
	}

	@Override
	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}

	@Override
	public String getProdCode() {
		return this.prodCode;
	}

	@Override
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	@Override
	public String getApiMethodName() {
		return "alipay.ecapiprod.drawndn.contract.get";
	}

	@Override
	public Map<String, String> getTextParams() {
		AlipayHashMap txtParams = new AlipayHashMap();
		txtParams.put("drawndn_no", this.drawndnNo);
		txtParams.put("entity_code", this.entityCode);
		txtParams.put("entity_name", this.entityName);
		txtParams.put("isv_code", this.isvCode);
		txtParams.put("org_code", this.orgCode);
		if (udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public void putOtherTextParam(String key, String value) {
		if (this.udfParams == null) {
			this.udfParams = new AlipayHashMap();
		}
		this.udfParams.put(key, value);
	}

	@Override
	public Class<AlipayEcapiprodDrawndnContractGetResponse> getResponseClass() {
		return AlipayEcapiprodDrawndnContractGetResponse.class;
	}


	@Override
	public boolean isNeedEncrypt() {

		return this.needEncrypt;
	}


	@Override
	public void setNeedEncrypt(boolean needEncrypt) {

		this.needEncrypt = needEncrypt;
	}

	@Override
	public AlipayObject getBizModel() {

		return this.bizModel;
	}


	@Override
	public void setBizModel(AlipayObject bizModel) {

		this.bizModel = bizModel;
	}


}
