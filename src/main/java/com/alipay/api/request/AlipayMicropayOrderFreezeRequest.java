package com.alipay.api.request;

import com.alipay.api.AlipayObject;
import com.alipay.api.AlipayRequest;
import com.alipay.api.internal.util.AlipayHashMap;
import com.alipay.api.response.AlipayMicropayOrderFreezeResponse;

import java.util.Date;
import java.util.Map;

/**
 * ALIPAY API: alipay.micropay.order.freeze request
 *
 * @author auto create
 * @since 1.0, 2016-06-06 17:49:00
 */
public class AlipayMicropayOrderFreezeRequest implements AlipayRequest<AlipayMicropayOrderFreezeResponse> {

	private AlipayHashMap udfParams; // add user-defined text parameters
	private String apiVersion = "1.0";

	/**
	 * 需要冻结金额，[0.01,2000]，必须是正数，最多只能保留小数点两位,单位是元
	 */
	private String amount;

	/**
	 * 冻结资金的到期时间，超过此日期，冻结金会自动解冻,时间要求是:[当前时间+24h,订购时间-8h] .
	 */
	private Date expireTime;

	/**
	 * 冻结备注,maxLength=40
	 */
	private String memo;

	/**
	 * 商户订单号,只能由字母和数字组成，最大长度32.此外部订单号与支付宝的冻结订单号对应,注意 应用id和订购者id和外部订单号必须保证唯一性。
	 */
	private String merchantOrderNo;

	/**
	 * 在解冻转账的时候的支付方式: NO_CONFIRM：不需要付款确认，调用接口直接扣款 PAY_PASSWORD: 在转账需要付款方用支付密码确认，才可以转账成功
	 */
	private String payConfirm;
	private String terminalType;
	private String terminalInfo;
	private String prodCode;
	private String notifyUrl;
	private String returnUrl;
	private boolean      needEncrypt = false;
	private AlipayObject bizModel    = null;

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Date getExpireTime() {
		return this.expireTime;
	}

	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}

	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMerchantOrderNo() {
		return this.merchantOrderNo;
	}

	public void setMerchantOrderNo(String merchantOrderNo) {
		this.merchantOrderNo = merchantOrderNo;
	}

	public String getPayConfirm() {
		return this.payConfirm;
	}

	public void setPayConfirm(String payConfirm) {
		this.payConfirm = payConfirm;
	}

	@Override
	public String getNotifyUrl() {
		return this.notifyUrl;
	}

	@Override
	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	@Override
	public String getReturnUrl() {
		return this.returnUrl;
	}

	@Override
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	@Override
	public String getApiVersion() {
		return this.apiVersion;
	}

	@Override
	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	@Override
	public String getTerminalType() {
		return this.terminalType;
	}

	@Override
	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@Override
	public String getTerminalInfo() {
		return this.terminalInfo;
	}

	@Override
	public void setTerminalInfo(String terminalInfo) {
		this.terminalInfo = terminalInfo;
	}

	@Override
	public String getProdCode() {
		return this.prodCode;
	}

	@Override
	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	@Override
	public String getApiMethodName() {
		return "alipay.micropay.order.freeze";
	}

	@Override
	public Map<String, String> getTextParams() {
		AlipayHashMap txtParams = new AlipayHashMap();
		txtParams.put("amount", this.amount);
		txtParams.put("expire_time", this.expireTime);
		txtParams.put("memo", this.memo);
		txtParams.put("merchant_order_no", this.merchantOrderNo);
		txtParams.put("pay_confirm", this.payConfirm);
		if (udfParams != null) {
			txtParams.putAll(this.udfParams);
		}
		return txtParams;
	}

	public void putOtherTextParam(String key, String value) {
		if (this.udfParams == null) {
			this.udfParams = new AlipayHashMap();
		}
		this.udfParams.put(key, value);
	}

	@Override
	public Class<AlipayMicropayOrderFreezeResponse> getResponseClass() {
		return AlipayMicropayOrderFreezeResponse.class;
	}


	@Override
	public boolean isNeedEncrypt() {

		return this.needEncrypt;
	}


	@Override
	public void setNeedEncrypt(boolean needEncrypt) {

		this.needEncrypt = needEncrypt;
	}

	@Override
	public AlipayObject getBizModel() {

		return this.bizModel;
	}


	@Override
	public void setBizModel(AlipayObject bizModel) {

		this.bizModel = bizModel;
	}


}
