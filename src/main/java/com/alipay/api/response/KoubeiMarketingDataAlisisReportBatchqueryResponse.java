package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.AlisisReport;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: koubei.marketing.data.alisis.report.batchquery response.
 *
 * @author auto create
 * @since 1.0, 2017-06-16 20:33:21
 */
public class KoubeiMarketingDataAlisisReportBatchqueryResponse extends AlipayResponse {

	private static final long serialVersionUID = 4383955497727767593L;

	/**
	 * 报表列表信息
	 */
	@ApiListField("report_list")
	@ApiField("alisis_report")
	private List<AlisisReport> reportList;

	/**
	 * 总记录数
	 */
	@ApiField("total_count")
	private String totalCount;

	public List<AlisisReport> getReportList() {
		return this.reportList;
	}

	public void setReportList(List<AlisisReport> reportList) {
		this.reportList = reportList;
	}

	public String getTotalCount() {
		return this.totalCount;
	}

	public void setTotalCount(String totalCount) {
		this.totalCount = totalCount;
	}

}
