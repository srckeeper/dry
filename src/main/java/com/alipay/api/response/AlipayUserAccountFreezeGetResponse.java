package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.domain.AccountFreeze;
import com.alipay.api.internal.mapping.ApiField;
import com.alipay.api.internal.mapping.ApiListField;

import java.util.List;

/**
 * ALIPAY API: alipay.user.account.freeze.get response.
 *
 * @author auto create
 * @since 1.0, 2016-08-11 15:48:05
 */
public class AlipayUserAccountFreezeGetResponse extends AlipayResponse {

	private static final long serialVersionUID = 7891269672332429338L;

	/**
	 * 冻结金额列表
	 */
	@ApiListField("freeze_items")
	@ApiField("account_freeze")
	private List<AccountFreeze> freezeItems;

	/**
	 * 冻结总条数
	 */
	@ApiField("total_results")
	private String totalResults;

	public List<AccountFreeze> getFreezeItems() {
		return this.freezeItems;
	}

	public void setFreezeItems(List<AccountFreeze> freezeItems) {
		this.freezeItems = freezeItems;
	}

	public String getTotalResults() {
		return this.totalResults;
	}

	public void setTotalResults(String totalResults) {
		this.totalResults = totalResults;
	}

}
