package com.alipay.api.response;

import com.alipay.api.AlipayResponse;
import com.alipay.api.internal.mapping.ApiField;

/**
 * ALIPAY API: alipay.mobile.public.message.single.send response.
 *
 * @author auto create
 * @since 1.0, 2017-06-15 14:45:00
 */
public class AlipayMobilePublicMessageSingleSendResponse extends AlipayResponse {

	private static final long serialVersionUID = 6877954486296848972L;

	/**
	 * 结果码
	 */
	@ApiField("code")
	private String code;

	/**
	 * 结果描述
	 */
	@ApiField("msg")
	private String msg;

	@Override
	public String getCode() {
		return this.code;
	}

	@Override
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String getMsg() {
		return this.msg;
	}

	@Override
	public void setMsg(String msg) {
		this.msg = msg;
	}

}
