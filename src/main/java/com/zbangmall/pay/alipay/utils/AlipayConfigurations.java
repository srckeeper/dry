package com.zbangmall.pay.alipay.utils;

import com.zbangmall.constant.resources.ResourcesConsts;

import java.io.InputStream;
import java.util.Properties;

/**
 * 支付宝支付配置对象
 */
public class AlipayConfigurations {

	public final static String GATE_URL = "https://openapi.alipay.com/gateway.do";
	private static Properties props;

	static {
		try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(ResourcesConsts.DRY_PAY)) {
			props = new Properties();
			props.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 支付宝支付公钥
	 */
	public static String getPublicKey() {
		return props.getProperty("alipay.public_key");
	}

	/**
	 * 支付宝支付私钥
	 */
	public static String getPrivateKey() {
		return props.getProperty("alipay.private_key");
	}

	/**
	 * 获取应用id
	 */
	public static String getAppid() {
		return props.getProperty("alipay.app_id");
	}

	/**
	 * 获取支付环境
	 */
	public static boolean getPayEnviroment() {
		String p = props.getProperty("pay.environment");
		if (p == null || "".equals(p)) {
			p = "dev";
		}
		return !"dev".equals(p);
	}

	/**
	 * 获取异步通知的回调地址
	 */
	public static String getNotifyUrl(String suffix) {
		if (!getPayEnviroment()) {
			suffix += "_test";
		}
		return props.getProperty("alipay.pay.notify_url." + suffix);
	}

	/**
	 * 获取同步通知的回调地址
	 */
	public static String getReturnUrl(String suffix) {
		if (!getPayEnviroment()) {
			suffix += "_test";
		}
		return props.getProperty("alipay.pay.return_url." + suffix);
	}
}
