package com.zbangmall.pay.wechat.utils;

import com.zbangmall.constant.resources.ResourcesConsts;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

/**
 * 微信配置文件，此类可修改
 */
public class WechatPayConfigurations {

	private static Properties props;

	static {
		try (InputStream in = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(ResourcesConsts.DRY_PAY)) {
			props = new Properties();
			props.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据后缀获取回调地址
	 */
	public static String getNotifyUrl(String suffix) {
		String notify_url = "";
		suffix = "wechat.pay.notify_url." + suffix;
		if (!getPayEnvironment()) {
			notify_url = props.getProperty(suffix + "_test");
		} else {
			notify_url = props.getProperty(suffix);
		}
		if (notify_url == null || "".equals(notify_url)) {
			throw new RuntimeException("回调地址不存在");
		}
		return notify_url;
	}

	/**
	 * 获取应用所在的环境类型
	 */
	public static boolean getPayEnvironment() {
		String p = props.getProperty("pay.environment");
		if (p == null || "".equals(p)) {
			p = "dev";
		}
		return !"dev".equals(p);
	}

	/**
	 * 获取支付密钥
	 */
	public static String getPayKey() {
		return props.getProperty("wechat.pay.key");
	}

	/**
	 * 应用id
	 */
	public static String getAppId() {
		return props.getProperty("wechat.appid");
	}

	/**
	 * 商户号
	 */
	public static String getMchId() {
		return props.getProperty("wechat.mch_id");
	}

	/**
	 * 应用名称
	 */
	public static String getAppName() throws UnsupportedEncodingException {
		return new String(props.getProperty("app.name").getBytes("ISO8859-1"), "UTF-8");
	}

	/**
	 * 获取微信退款证书所在路径
	 */
	public static String getRefundCertificatePath() {
		return props.getProperty("wechat.pay.refund.certificate.path");
	}

	/**
	 * 移动应用的退款密码
	 */
	public static String getRefundCertificatePassword() {
		return props.getProperty("wechat.pay.refund.certificate.password");
	}

}
