package com.zbangmall.common;


import com.zbangmall.constant.resources.ResourcesConsts;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
	/**
	 * 属性文件的属性和值
	 */
	public static Properties propsWechat;
	public static Properties propsPay;

	static {

		try (InputStream in = PropertiesUtil.class.getClassLoader().getResourceAsStream(ResourcesConsts.DRY_WECHAT)) {
			propsWechat = new Properties();
			propsWechat.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static {

		try (InputStream in = PropertiesUtil.class.getClassLoader().getResourceAsStream(ResourcesConsts.DRY_PAY)) {
			propsPay = new Properties();
			propsPay.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
