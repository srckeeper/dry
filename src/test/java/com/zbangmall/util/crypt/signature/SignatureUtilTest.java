package com.zbangmall.util.crypt.signature;

import com.zbangmall.util.crypt.asymmetric.RsaCrypt;
import org.junit.Test;

import java.security.KeyPair;

public class SignatureUtilTest {
	@Test
	public void test() throws Exception {
		String  str        = "asdfasdf";
		KeyPair keyPair    = RsaCrypt.getKeyPair();
		String  resultStr  = SignatureUtil.signBySHA256withDSA(keyPair.getPrivate(), str);
		Boolean resultBool = SignatureUtil.verifyBySHA256withDSA(keyPair.getPublic(), resultStr, str + "");

		System.out.println(resultStr);
		System.out.println(resultBool);


	}

}